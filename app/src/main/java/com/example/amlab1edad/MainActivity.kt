package com.example.amlab1edad

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.amlab1edad.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        events()
    }

    private fun events(){
        binding.btnProcess.setOnClickListener { verifyAge() }
    }

    private fun verifyAge(){
        binding.apply {
            val age = edtAge.text.toString().trim().toInt()
            if(age>=18)tvResult.text="Usted es mayor de edad" else tvResult.text="Usted es menor de edad"
        }
    }

}